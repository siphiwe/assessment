
## Recommended installation steps
In this document, I specify software versions that I Developed and tested on, but it should 
be possible to use fairly recent versions. The technologies are as follows:

### Front end
* Angular 10.1.7 (`npm install -g @angular/cli@10.1.7`)
* To install dependencies: `npm install`

### Back end
You should be able to install all python related dependencies using:
`python -m pip install -r requirements.txt`. I used an existing virtual environment
that contains dependencies that are not required by this project, but the effect of 
that is negligible. Key technologies are
* Django 3.1.7 (You can install it from)
* Python 3.9.2 (You can install it from `https://www.python.org/downloads/`)
* SQLite3 (in-memory mode)

### To run back end tests (not a requirement):
`../bin/python3 manage.py test`

### Build instructions (command line based):
Install back and front end code bases:
* Extract assessment_backend.zip into a folder
* Change directory into the resultant "assessment_backend" directory: `cd assessment_backend`
* Run the Django development web server:
`python3 manage.py runserver`. You might need to specify a path to your python 3 installation if it's not in the PATH environment variable.
* Extract assessment.zip into a folder
* Change directory into the resultant "assessment" directory: `cd assessment`
* Run the front end server: `ng serve -co`
* Browse through the app: `http://localhost:4200/`

### Misc
* As per spec, the user gets to populate the three screens and submit. As a convenience,
I added an additional screen to list the added persons: `http://localhost:4200/list`.
* On the list page, I added a link to download the list of users as a CSV file,
but it can be viewed directly off `http://127.0.0.1:8000/persons/download_persons`.
* Although the codebase uses descriptive names for methods and variables, as well
  as some commentary for added clarity; but if anything is unclear, I can be 
  contacted at siphiwe@meluleki.co.za or 082 560 5911.
