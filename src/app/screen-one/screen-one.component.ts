import { defaultScreenOne }                   from './../models/person.model';
import { ApiService }                         from './../services/api';
import { Component, OnInit, ViewChild }       from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router }                             from '@angular/router';
import { Observable }                         from 'rxjs';
import { Store }                              from '@ngrx/store';

import { updateScreenOne }                    from './../services/rxjs/actions';
import { StorageService }                     from './../services/storage.service';
import { ScreenOne }                          from '../models/person.model';

@Component({
  selector: 'app-screen-one',
  templateUrl: './screen-one.component.html',
  styleUrls: ['./screen-one.component.scss']
})
export class ScreenOneComponent implements OnInit {
  public screenOneForm : FormGroup;
  @ViewChild('focusField') focusField;

  screenOneSlice: Observable<ScreenOne>;
  screenOne: ScreenOne = defaultScreenOne;
  personCounter: number;
  constructor(
              private apiService    : ApiService,
              private formBuilder   : FormBuilder,
              private router        : Router,
              public storageService : StorageService,
              private store         : Store<{"screenOneSlice": ScreenOne}>) {
                this.getAllPersonsCount();
              }

  ngOnInit(): void {
    this.store.select(state => state).subscribe(data => {
      this.screenOne = data["screenOneReducer"];
      this.screenOneForm = this.formBuilder.group({
        firstName   : [this.screenOne.firstName, [Validators.required]],
        lastName    : [this.screenOne.lastName ,  Validators.required],
        // If cellNumber is optional, this regex check can be in a custom validator that allows empty string
        cellNumber  : [this.screenOne.cellNumber, Validators.pattern(/^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/)],
      });
    });
    /* this.screenOneSlice = this.store.select("screenOneSlice");
    this.screenOneSlice.subscribe((data: ScreenOne) => {
      console.log("Data:", data);
      // this.screenOne = data==null?defaultScreenOne:data;
      // this.screenOneForm = this.formBuilder.group({
      //   firstName   : [this.screenOne.firstName, [Validators.required]],
      //   lastName    : [this.screenOne.lastName ,  Validators.required],
      //   // If cellNumber is optional, this regex check can be in a custom validator that allows empty string
      //   cellNumber  : [this.screenOne.cellNumber, Validators.pattern(/^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/)],
      // });
    }); */
    setTimeout(() => {this.focusField.nativeElement.focus()},300);
  }

  goToScreenTwo() {
    console.log
    this.screenOne = {
      "firstName" : this.screenOneForm.value.firstName,
      "lastName"  : this.screenOneForm.value.lastName,
      "cellNumber": this.screenOneForm.value.cellNumber
    }
    this.store.dispatch(updateScreenOne(this.screenOne));
    this.router.navigate(['/two']);
  }

  getAllPersonsCount() {
    this.apiService.getPersonsCount().toPromise()
    .then((response) => {
      this.personCounter = response["count"];
    })
    .catch(err => {
      console.error("Counldn't retrieve persons count — ", err)
    });
  }
}
