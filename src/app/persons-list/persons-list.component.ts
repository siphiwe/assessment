import { Component, OnInit }  from '@angular/core';
import { Router }             from '@angular/router';

import { ApiService }         from '../services/api';
import { StorageService }     from './../services/storage.service';

@Component({
  selector: 'app-persons-list',
  templateUrl: './persons-list.component.html',
  styleUrls: ['./persons-list.component.scss']
})
export class PersonsListComponent implements OnInit {

  constructor(
              private apiService: ApiService,
              private router: Router,
              public storageService: StorageService) {
                this.getAllPersons();
              }

  ngOnInit(): void {
  }

  getAllPersons() {
    this.apiService.getAllPersons().toPromise()
    .then((persons: []) => {
      this.storageService.persons = [];
      persons.forEach(person => {
        this.storageService.persons.push({
          "firstName"            : person["first_name"],
          "lastName"             : person["last_name"],
          "cellNumber"           : person["cell_number"],
          "physicalAddressLine1" : person["physical_address_line_1"],
          "physicalAddressLine2" : person["physical_address_line_2"],
          "physicalAddressLine3" : person["physical_address_line_3"],
          "postalAddressLine1"   : person["postal_address_line_1"],
          "postalAddressLine2"   : person["postal_address_line_2"],
          "postalAddressLine3"   : person["postal_address_line_3"],
          "comments"             : person["comments"]
        });
      })
    })
    .catch(err => {
      console.error("Counldn't retrieve persons list:", err)
    });
  }

  goToScreenOne() {
    this.router.navigate(['/']);
  }
}
