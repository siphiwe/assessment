export interface Person {
  firstName           : string,
  lastName            : string,
  cellNumber          : string,
  physicalAddressLine1: string,
  physicalAddressLine2: string,
  physicalAddressLine3: string,
  postalAddressLine1  : string,
  postalAddressLine2  : string,
  postalAddressLine3  : string,
  comments            : string
}

export interface ScreenOne {
  firstName           : string,
  lastName            : string,
  cellNumber          : string
}
export interface ScreenTwo {
  physicalAddressLine1: string,
  physicalAddressLine2: string,
  physicalAddressLine3: string,
  postalAddressLine1  : string,
  postalAddressLine2  : string,
  postalAddressLine3  : string
}

export interface ScreenThree {
  comments            : string
}

export const defaultScreenOne: ScreenOne = {
  firstName           : "",
  lastName            : "",
  cellNumber          : "",
}

export const defaultScreenTwo: ScreenTwo = {
  physicalAddressLine1: "",
  physicalAddressLine2: "",
  physicalAddressLine3: "",
  postalAddressLine1  : "",
  postalAddressLine2  : "",
  postalAddressLine3  : "",
}

export const defaultScreenThree: ScreenThree = {
  comments            : ""
}

export const defaultPerson: Person = {
  firstName           : "",
  lastName            : "",
  cellNumber          : "",
  physicalAddressLine1: "",
  physicalAddressLine2: "",
  physicalAddressLine3: "",
  postalAddressLine1  : "",
  postalAddressLine2  : "",
  postalAddressLine3  : "",
  comments            : ""
};
