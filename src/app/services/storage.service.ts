import { Injectable } from '@angular/core';
import { Person }     from './../models/person.model';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  person: Person = {
    firstName           : "",
    lastName            : "",
    cellNumber          : "",
    physicalAddressLine1: "",
    physicalAddressLine2: "",
    physicalAddressLine3: "",
    postalAddressLine1  : "",
    postalAddressLine2  : "",
    postalAddressLine3  : "",
    comments            : ""
  };
  persons: Array<Person> = [];

  constructor() { }

}
