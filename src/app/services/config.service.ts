import { HttpErrorResponse }  from '@angular/common/http';
import { Injectable }         from '@angular/core';
import { throwError }         from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor() { }

  handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred.
      return throwError('please check your internet connection and try again [' + error.statusText + ']');
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
        return throwError('please check your input and try again [' + error.statusText + ']');
    }
  }
}
