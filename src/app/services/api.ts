import { catchError }               from 'rxjs/operators';
import { ConfigService }            from './config.service';
import { Injectable }               from '@angular/core';
import { HttpClient, HttpHeaders }  from '@angular/common/http';
import { Store }                    from '@ngrx/store';

 const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/x-www-form-urlencoded',
  })
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  hostUrl = "http://127.0.0.1:8000/persons/";
  constructor(
              private configService: ConfigService,
              private http: HttpClient,
              private store: Store<{ person:string }>) {
  }

  createPerson() {
    let body = this._getBody();
    return this.http.post(this.hostUrl + "create_person", body, httpOptions)
    .pipe(
      catchError(this.configService.handleError)
    );
  }

  _getBody() {
    let body = new URLSearchParams();
    this.store.select(state => state).subscribe(data => {
      let screenOne   = data["screenOneReducer"];
      let screenTwo   = data["screenTwoReducer"];
      let screenThree = data["screenThreeReducer"];
      body.set('first_name'             , screenOne.firstName);
      body.set('last_name'              , screenOne.lastName);
      body.set('cell_number'            , screenOne.cellNumber);
      body.set('postal_address_line_1'  , screenTwo.postalAddressLine1);
      body.set('postal_address_line_2'  , screenTwo.postalAddressLine2);
      body.set('postal_address_line_3'  , screenTwo.postalAddressLine3);
      body.set('physical_address_line_1', screenTwo.physicalAddressLine1);
      body.set('physical_address_line_2', screenTwo.physicalAddressLine2);
      body.set('physical_address_line_3', screenTwo.physicalAddressLine3);
      body.set('comments'               , screenThree.comments);
    });
    return body.toString();
  }

  getAllPersons() {
    return this.http.get(this.hostUrl + "get_persons")
    .pipe(
      catchError(this.configService.handleError)
    );
  }

  getPersonsCount() {
    return this.http.get(this.hostUrl + "get_persons_count")
    .pipe(
      catchError(this.configService.handleError)
    );
  }
}