import { Action, createAction, props } from '@ngrx/store';
import { ScreenOne, ScreenTwo, ScreenThree } from './../../models/person.model';

export const UPDATE_SCREEN_ONE    = '[PERSON] Update Screen One';
export const UPDATE_SCREEN_TWO    = '[PERSON] Update Screen Two';
export const UPDATE_SCREEN_THREE  = '[PERSON] Update ScreenThree';

export const updateScreenOne = createAction(
  '[Person] Update Screen One',
  props< ScreenOne >()
);
export const updateScreenTwo = createAction(
  '[Person] Update Screen Two',
  props< ScreenTwo >()
);
export const updateScreenThree = createAction(
  '[Person] Update Screen Three',
  props< ScreenThree >()
);


export class UpdateScreenOne implements Action {
    readonly type = UPDATE_SCREEN_ONE;

    constructor(public payload: ScreenOne) {}
}

export class UpdateScreenTwo implements Action {
    readonly type = UPDATE_SCREEN_TWO;

    constructor(public payload: ScreenTwo) {}
}

export class UpdateScreenThree implements Action {
    readonly type = UPDATE_SCREEN_THREE;

    constructor(public payload: ScreenThree) {}
}
