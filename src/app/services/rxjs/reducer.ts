import {
  defaultScreenOne,
  defaultScreenTwo,
  defaultScreenThree
}                                            from '../../models/person.model';
import { ScreenOne, ScreenTwo, ScreenThree } from './../../models/person.model';
import * as PersonActions                    from './../rxjs/actions';
import { createReducer, on }                 from '@ngrx/store';

const _screenOneReducer = createReducer(
  defaultScreenOne,
  on(PersonActions.updateScreenOne, (state, action) => {
    return Object.assign(action);
  }),
);
export function screenOneReducer(state: ScreenOne=defaultScreenOne, action: PersonActions.UpdateScreenOne): ScreenOne {
  return _screenOneReducer(state, action);
}

const _screenTwoReducer = createReducer(
  defaultScreenTwo,
  on(PersonActions.updateScreenTwo, (state, action) => {
    return Object.assign(action);
  }),
);
export function screenTwoReducer(state: ScreenTwo=defaultScreenTwo, action: PersonActions.UpdateScreenTwo): ScreenTwo {
  return _screenTwoReducer(state, action);
}

const _screenThreeReducer = createReducer(
  defaultScreenThree,
  on(PersonActions.updateScreenThree, (state, action) => {
    return Object.assign(action);
  }),
);
export function screenThreeReducer(state: ScreenThree=defaultScreenThree, action: PersonActions.UpdateScreenThree): ScreenThree {
  return _screenThreeReducer(state, action);
}
