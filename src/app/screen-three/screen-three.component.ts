import { Store }                                                from '@ngrx/store';
import { Component, OnInit, ViewChild }       from '@angular/core';
import { Router }                             from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import {
  defaultScreenOne,
  defaultScreenTwo,
  defaultScreenThree,
  ScreenThree
}                      from './../models/person.model';
import {
  updateScreenOne,
  updateScreenTwo,
  updateScreenThree }  from './../services/rxjs/actions';
import { ApiService }  from './../services/api';


@Component({
  selector: 'app-screen-three',
  templateUrl: './screen-three.component.html',
  styleUrls: ['./screen-three.component.scss']
})
export class ScreenThreeComponent implements OnInit {
  public screenThreeForm : FormGroup;
  @ViewChild('focusField') focusField;
  screenThree: ScreenThree = defaultScreenThree;
  submitted: boolean       = false;
  successMessage           = "";
  errorMessage             = "";

  constructor(
              private apiService     : ApiService,
              private formBuilder    : FormBuilder,
              private router         : Router,
              private store          : Store<{ count: number }>) { }

  ngOnInit(): void {
    this.store.select(state => state).subscribe(data => {
      this.screenThree = data["screenThreeReducer"];
      this.screenThreeForm = this.formBuilder.group({
        comments: [this.screenThree.comments, [Validators.required]]
      });
    });
    setTimeout(() => {this.focusField.nativeElement.focus()}, 300);
  }

  submit() {
    this.dispatch();
    this.errorMessage = "";
    this.apiService.createPerson().toPromise()
    .then(data => {
      this.successMessage = data["result"];
      this.store.dispatch(updateScreenOne(defaultScreenOne));
      this.store.dispatch(updateScreenTwo(defaultScreenTwo));
      this.store.dispatch(updateScreenThree(defaultScreenThree));
      this.screenThreeForm.reset();
      this.submitted = true;
      setTimeout(() => {this.goToScreenOne()}, 2000);
    })
    .catch(err => {
      this.errorMessage = "Couldn't create person — " + err;
      this.submitted = true;
      console.error("Counldn't create person —", err);
    });
  }

  dispatch() {
    this.screenThree = { "comments" : this.screenThreeForm.value.comments, }
    this.store.dispatch(updateScreenThree(this.screenThree));
  }

  goToScreenOne() {
    this.router.navigate(['/']);
  }
}
