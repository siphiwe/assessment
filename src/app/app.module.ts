import { BrowserModule }        from '@angular/platform-browser';
import { HttpClientModule }     from '@angular/common/http';
import { NgModule }             from '@angular/core';
import { ReactiveFormsModule }  from '@angular/forms';
import { StoreModule }          from '@ngrx/store';

import { ApiService }           from './services/api';
import { AppRoutingModule }     from './app-routing.module';
import { AppComponent }         from './app.component';
import { BackButtonDirective }  from './directives/back-button.directive';
import {
  screenOneReducer,
  screenTwoReducer,
  screenThreeReducer,
}                               from './services/rxjs/reducer';
import { ScreenOneComponent }   from './screen-one/screen-one.component';
import { ScreenTwoComponent }   from './screen-two/screen-two.component';
import { ScreenThreeComponent } from './screen-three/screen-three.component';
import { PersonsListComponent } from './persons-list/persons-list.component';
import { StoreDevtoolsModule }  from '@ngrx/store-devtools';

import { environment }          from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    ScreenOneComponent,
    ScreenTwoComponent,
    BackButtonDirective,
    ScreenThreeComponent,
    PersonsListComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forRoot({
      screenOneReducer  : screenOneReducer,
      screenTwoReducer  : screenTwoReducer,
      screenThreeReducer: screenThreeReducer
    }),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
