import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ScreenOneComponent }   from './screen-one/screen-one.component';
import { ScreenTwoComponent }   from './screen-two/screen-two.component';
import { ScreenThreeComponent } from './screen-three/screen-three.component';
import { PersonsListComponent } from './persons-list/persons-list.component';

const routes: Routes = [
  {path: ''     , component: ScreenOneComponent },
  {path: 'one'  , component: ScreenOneComponent },
  {path: 'two'  , component: ScreenTwoComponent },
  {path: 'three', component: ScreenThreeComponent },
  {path: 'list' , component: PersonsListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
