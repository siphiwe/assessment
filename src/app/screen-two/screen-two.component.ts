import { Component, OnInit, ViewChild }       from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router }                             from '@angular/router';
import { Store }                              from '@ngrx/store';

import { ScreenTwo,defaultScreenTwo }         from './../models/person.model';
import { updateScreenTwo }                    from './../services/rxjs/actions';

@Component({
  selector    : 'app-screen-two',
  templateUrl : './screen-two.component.html',
  styleUrls   : ['./screen-two.component.scss']
})
export class ScreenTwoComponent implements OnInit {
  public screenTwoForm : FormGroup;
  screenTwo: ScreenTwo = defaultScreenTwo;
  @ViewChild('focusField') focusField;

  constructor(
              private formBuilder   : FormBuilder,
              private router        : Router,
              private store         : Store) { }

  ngOnInit(): void {
    this.store.select(state => state).subscribe(data => {
      this.screenTwo = data["screenTwoReducer"];
      this.screenTwoForm = this.formBuilder.group({
        physicalAddressLine1   : [this.screenTwo.physicalAddressLine1, [Validators.required]],
        physicalAddressLine2   : [this.screenTwo.physicalAddressLine2, [Validators.required]],
        physicalAddressLine3   : [this.screenTwo.physicalAddressLine3, [Validators.required]],
        postalAddressLine1     : [this.screenTwo.postalAddressLine1  ,  Validators.required],
        postalAddressLine2     : [this.screenTwo.postalAddressLine2  ,  Validators.required],
        postalAddressLine3     : [this.screenTwo.postalAddressLine3  ,  Validators.required],
      });
    });
    setTimeout(() => {this.focusField.nativeElement.focus()},300);
  }

  dispatch() {
    this.screenTwo = {
      "physicalAddressLine1"  : this.screenTwoForm.value.physicalAddressLine1,
      "physicalAddressLine2"  : this.screenTwoForm.value.physicalAddressLine2,
      "physicalAddressLine3"  : this.screenTwoForm.value.physicalAddressLine3,
      "postalAddressLine1"    : this.screenTwoForm.value.postalAddressLine1,
      "postalAddressLine2"    : this.screenTwoForm.value.postalAddressLine2,
      "postalAddressLine3"    : this.screenTwoForm.value.postalAddressLine3,
    }
    this.store.dispatch(updateScreenTwo(this.screenTwo));
  }

  goToScreenThree() {
    this.dispatch();
    this.router.navigate(['/three']);
  }
}
